import pathlib
import sys

from fastapi import FastAPI

sys.path.insert(0, str(pathlib.Path(__file__).parent.parent.resolve()))
from crud import run, get_status_full

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "I am OK"}


@app.get("/check")
async def check():
    return run()


@app.get("/status/db/0")
async def get_full_status_db():
    return get_status_full()
