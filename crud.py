import models

from database import SessionLocal
from calculate_rating import Rank


class CRUD:
    def __init__(self):
        self.users = []
        self.rating_users = []
        self.new_set_evals = None
        self.db = None
        self.evals_intermed = {}
        self.evals_overall = {}

        self.old_rating_intermed = {}
        self.old_rating_overall = {}

        self.new_rating_intermed = {}
        self.new_rating_overall = {}

    def get_db(func):
        """
        вызов сессии к БД
        :param func: function
        :return:
        """

        def wrapper(self, *args, **kwargs):
            self.db = SessionLocal()
            ret = func(self, *args, **kwargs)
            self.db.close()
            return ret

        return wrapper

    @get_db
    def get_new_setevals(self):
        """
        получение списка данных по новым оценкам
        :return: объект БД 
        """
        self.new_set_evals = self.db.query(models.SetEval). \
            filter(models.SetEval.fresh == True).all()

    def get_count_new_evals(self):
        """
        проверка новых оценок
        :return:
        """
        if not self.new_set_evals:
            return False
        else:
            return True

    @get_db
    def get_user_with_new_evals(self):
        """
        выборка из таблицы rating тех данных, где есть
        пользователи из таблицы set_eval
        :return:
        """
        for new_eval in self.new_set_evals:
            self.rating_users.append(self.db.query(models.Rating). \
                                     filter(models.
                                            Rating.lk_user_id == new_eval.
                                            to_user_id). \
                                     first())

    @get_db
    def get_full_status(self):
        """
        получение данных по пользователям
        полный статус
        :return:
        """
        self.rating_users.append(self.db.query(models.Rating).all())
        return self.rating_users

    @get_db
    def get_users(self):
        """
        получение списка юзеров у которых есть оценки
        :return:
        """
        for new_eval in self.new_set_evals:
            self.users.append(new_eval.to_user_id)

    def get_evals(self):
        """
        получаем данные по новым оценкам в формате
        {user: [evals]}
        :return:
        """
        for new_eval in self.new_set_evals:
            self.evals_intermed.update({new_eval.to_user_id:
                [
                    new_eval.like_1,
                    new_eval.like_2,
                    new_eval.like_3,
                    new_eval.like_4,
                    new_eval.like_5,
                ]})
            self.evals_overall.update({new_eval.to_user_id:
                                           new_eval.overall_rating
                                       })

    def get_old_rating(self):
        for rating_user in self.rating_users:
            self.old_rating_intermed.update({rating_user.lk_user_id: [
                [rating_user.param_1, rating_user.likes_1],
                [rating_user.param_2, rating_user.likes_2],
                [rating_user.param_3, rating_user.likes_3],
                [rating_user.param_4, rating_user.likes_4],
                [rating_user.param_5, rating_user.likes_5],
            ]})
            self.old_rating_overall.update({rating_user.lk_user_id: [
                rating_user.overall_rating,
                rating_user.overall_likes
            ]})

    def get_old_general_value(self, user_id):
        """
        получение списка значений старого рейтинга по
        юзеру общего
        :param user_id:
        :return:
        """
        return self.old_rating_overall[user_id]

    def get_old_intermed_values(self, user_id):
        """
        получение списка значений старого рейтинга по
        юзеру по параметрам
        :param user_id:
        :return:
        """
        return self.old_rating_intermed[user_id]

    def get_new_general_star(self, user_id):
        """
        получение списка значений новых оценок по
        юзеру по общему
        :param user_id:
        :return:
        """
        return self.evals_overall[user_id]

    def get_new_intermed_star(self, user_id):
        """
        получение списка значений новых оценок по
        юзеру по параметрам
        :param user_id:
        :return:
        """
        return self.evals_intermed[user_id]

    def set_new_values_rating(self, user_id, new_general_value, new_intermed_values):
        """
        установка новых значений рейтингов
        :param user_id:
        :param new_general_value: список
        :param new_intermed_values: список
        :return:
        """
        self.new_rating_intermed.update({user_id: new_intermed_values})
        self.new_rating_overall.update({user_id: new_general_value})

    @get_db
    def set_new_rating(self):
        """
        Установка новых значений в рейтинг
        :return:
        """
        for user in self.rating_users:
            id = user.lk_user_id
            user.overall_rating = self.new_rating_overall.get(id)[0]
            user.overall_likes = self.new_rating_overall.get(id)[1]
            user.param_1 = self.new_rating_intermed.get(id)[0][0]
            user.likes_1 = self.new_rating_intermed.get(id)[0][1]
            user.param_2 = self.new_rating_intermed.get(id)[1][0]
            user.likes_2 = self.new_rating_intermed.get(id)[1][1]
            user.param_3 = self.new_rating_intermed.get(id)[2][0]
            user.likes_3 = self.new_rating_intermed.get(id)[2][1]
            user.param_4 = self.new_rating_intermed.get(id)[3][0]
            user.likes_4 = self.new_rating_intermed.get(id)[3][1]
            user.param_5 = self.new_rating_intermed.get(id)[4][0]
            user.likes_5 = self.new_rating_intermed.get(id)[4][1]
            self.db.add(user)
            self.db.commit()

    @get_db
    def set_fresh_to_false_into_set_eval(self):
        """
        Изменение флага свежей оценки на старую
        :return:
        """
        for set_eval in self.new_set_evals:
            set_eval.fresh = False
            self.db.add(set_eval)
            self.db.commit()


def get_status_full():
    try:
        crud = CRUD()
        return crud.get_full_status()

    except Exception as e:
        # при ошибке сообщаем API
        return {"error": 1,
                "message": "No get, error"}


def run():
    """
    Основная программа
    :return: ответ по выполнению {"error": , "message": ""}
    """
    try:
        crud = CRUD()

        # получаем новые оценки
        crud.get_new_setevals()

        # если нет новых оценок, то прекращаем работу
        if not crud.get_count_new_evals():
            return {"error": 0,
                    "message": "No fresh"}

        # формируем список новых оценок
        crud.get_evals()

        # получаем юзеров по которым нужно считать оценки
        crud.get_user_with_new_evals()

        # получаем старые рейтинги по юзерам, у которых есть
        # новые оценки
        crud.get_old_rating()

        # получаем список юзеров
        crud.get_users()

        # создаем объекты Rank и запускаем расчет
        for user in crud.users:
            rank = Rank(
                old_general_value=crud.get_old_general_value(user_id=user),
                old_intermed_values=crud.get_old_intermed_values(user_id=user),
                new_general_star=crud.get_new_general_star(user_id=user),
                new_intermed_star=crud.get_new_intermed_star(user_id=user)
            )
            rank.run()

            # получаем новые значения ретингов
            new_general_value, new_intermed_values = rank.get_new_params()
            crud.set_new_values_rating(user_id=user,
                                       new_general_value=new_general_value,
                                       new_intermed_values=new_intermed_values)

        # устанавливаем новые значения рейтингов
        crud.set_new_rating()

        # обновляем флаги fresh = False в set_eval
        crud.set_fresh_to_false_into_set_eval()

        # по завершению выдаем ответ API
        return {"error": 0,
                "message": "Complete"}

    except Exception as e:
        # при ошибке сообщаем API
        return {"error": 1,
                "message": "No check, error"}


if __name__ == '__main__':
    run()
