import os

from crontab import CronTab, CronItem
from configparser import ConfigParser


class Scheduler:
    def __init__(self):
        # иницилизация конфигуратора
        self.config = ConfigParser()
        # определение текущей директории (там и должен быть исполняемый файл)
        self.path_current_dir = os.getcwd()

        # иницилизация cron_fhc_rating-a
        self.cron = CronTab(user=os.getlogin())
        # self.cron_fhc_rating = CronTab(user='newuser')

    def schedule(func):
        """
        Обертка планировщика
        :param func:
        :return:
        """

        def wrapper(self, *args, **kwargs):
            ret = func(self, *args, **kwargs)
            self.cron.write()
            return ret

        return wrapper

    def set_ini(self, file_ini):
        self.config.read(filenames=file_ini)

    def get_path(self, file):
        """
        Задание пути к файлу который необходимо запускать
        :rtype: object
        """
        return os.path.join(self.path_current_dir, file)

    @schedule
    def add_task(self, path_to_job, comment):
        """
        Дает задание на запуск с частотой
        :param comment: комментарий, для поиска, если удалить задачу нужно будет
        :param path_to_job: str
        :return:
        """
        job = self.cron.new(command='python3 ' + path_to_job,
                            comment=comment)
        job.hour.every(12)

    @schedule
    def remove_task(self, comment):
        """
        Убирает задание
        :param comment: по какому комментарию искать
        :param path_to_job: str
        :return:
        """
        for job in self.cron:
            if job.comment == comment:
                self.cron.remove(job)


def run():
    scheduler = Scheduler()
    scheduler.set_ini('setting_crud.ini')
    scheduler.add_task(path_to_job=scheduler.
                       get_path(scheduler.config['JOB']['rating']),
                       comment=scheduler.config['COMMENT']['for_rating'])
    # scheduler.remove_task(comment=scheduler.config['COMMENT']['for_rating'])


if __name__ == '__main__':
    run()
