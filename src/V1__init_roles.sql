--// таблица ролей пользователей:
--// заказчик, подрядчик, администратор FHC(for house club), менеджер FHC
CREATE TABLE IF NOT EXISTS roles
(
    role_id                 serial PRIMARY KEY,
    role_name               varchar(50) NOT NULL
);

INSERT INTO roles (role_name) VALUES
('ROLE_ADMIN'),
('ROLE_MANAGER');




