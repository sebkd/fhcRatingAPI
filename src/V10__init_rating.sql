create table IF NOT EXISTS rating
(
    rating_id      bigserial primary key,
    lk_user_id     UUID not null,
    overall_rating float,
    overall_likes  int,
    param_1        float,
    likes_1        int,
    param_2        float,
    likes_2        int,
    param_3        float,
    likes_3        int,
    param_4        float,
    likes_4        int,
    param_5        float,
    likes_5        int,
    foreign key (lk_user_id) references users (user_id)
);

insert into rating (lk_user_id, overall_rating, overall_likes, param_1, likes_1, param_2, likes_2, param_3, likes_3,
                    param_4, likes_4, param_5, likes_5)
values ('797e5399-e280-44bc-b9ae-d422ee45afde', 3.8, 15, 5, 24, 3, 34, 1, 4, 5, 67, 2, 13);
insert into rating (lk_user_id, overall_rating, overall_likes, param_1, likes_1, param_2, likes_2, param_3, likes_3,
                    param_4, likes_4, param_5, likes_5)
values ('22dc87ab-2cea-43ed-ab20-12def235bccc', 3.75, 16, 5, 24, 2, 34, 1, 4, 5, 67, 2, 13);
ALTER TABLE portfolio
    ADD COLUMN rating bigint;

UPDATE portfolio
SET rating=5
WHERE id = 1;

