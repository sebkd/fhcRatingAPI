create table IF NOT EXISTS set_eval
(
    seteval_id     bigserial primary key,
    from_user_id   UUID not null,
    to_user_id     UUID not null,
    fresh          boolean,
    overall_rating int,
    like_1         int,
    like_2         int,
    like_3         int,
    like_4         int,
    like_5         int,
    foreign key (from_user_id) references users (user_id),
    foreign key (to_user_id) references users (user_id)
);

insert into set_eval (from_user_id, to_user_id, fresh, overall_rating,
                      like_1, like_2, like_3, like_4, like_5)
values ('797e5399-e280-44bc-b9ae-d422ee45afde',
        '22dc87ab-2cea-43ed-ab20-12def235bccc', True, 0, 5, 3, 2, 1, 3);
insert into set_eval (from_user_id, to_user_id, fresh, overall_rating,
                      like_1, like_2, like_3, like_4, like_5)
values ('22dc87ab-2cea-43ed-ab20-12def235bccc', '797e5399-e280-44bc-b9ae-d422ee45afde', True,
        3, 0, 0, 0, 0, 0);