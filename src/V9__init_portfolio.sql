create table IF NOT EXISTS photo (
        id              bigserial primary key,
        photo_path      varchar(100),
        external_id     varchar(255),
        description     varchar(1000)
);

insert into photo (photo_path) values ('1.jpg');
insert into photo (photo_path) values ('2.jpg');

create table IF NOT EXISTS photo_album (
        id              bigserial primary key,
        title           varchar(255),
        description     varchar(1000)
);

insert into photo_album (title, description) values
('Arbat', 'Квартира 150м2'),
('Tverskaya', 'Квартира 250м2');

create table IF NOT EXISTS photos_albums (
        photo_id        bigint references photo (id),
        album_id        bigint references photo_album (id),
        primary key (photo_id, album_id)
);

insert into photos_albums (photo_id, album_id) values (1, 1);
insert into photos_albums (photo_id, album_id) values (2, 1);

create table IF NOT EXISTS portfolio (
        id              bigserial primary key,
        client_id       UUID not null,
        description     varchar(1000)
);

insert into portfolio (description, client_id) values
('Капитальный ремонт квартиры площадью 230м2. Демонтажные работы, монтаж новых стен, полов, потолоков', 'dc4d798b-44e1-4edf-9e8e-8dc8043ab472'),
('Автоматизация квартиры 135м2','94634392-9bf3-4329-be9e-c487c1da4d2d'),
('Дизайн и архитектурный проект коттеджа Новая рига','7895f5bd-a201-4709-8886-5acdfe824163'),
('Изготовление мебели и штор', '5a4e237b-6413-4a1d-8eee-9cc5754661e8');

create table IF NOT EXISTS portfolio_albums (
        portfolio_id    bigint references portfolio(id),
        album_id        bigint references photo_album(id),
        primary key (portfolio_id, album_id)
);

insert into portfolio_albums (portfolio_id, album_id) values
(1, 1),
(1, 2);

create table IF NOT EXISTS portfolio_reviews(
        portfolio_id    bigint references portfolio(id),
        review_id       bigint references review(id),
        primary key (portfolio_id, review_id)
);

insert into portfolio_reviews(portfolio_id, review_id) values (1, 1);
insert into portfolio_reviews(portfolio_id, review_id) values (1, 2);