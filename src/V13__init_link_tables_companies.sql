--таблица связей компании и специализаций
create table IF NOT EXISTS company_specializations (
    company_id              UUID not null,
    specialization_id       bigint not null,
    primary key (company_id, specialization_id),
    foreign key (company_id) references companies(company_id),
    foreign key (specialization_id) references specializations (id)
);

--//таблица связей компаний
create table IF NOT EXISTS connection_contractor_customers
(
    contractor_id          UUID,
    customer_id            UUID,
    status_connection      int, --//статус: подрядчик, поставщик
    primary key (contractor_id, customer_id),
    foreign key (contractor_id) references companies (company_id),
    foreign key (customer_id) references companies (company_id),
    foreign key (status_connection) references roles (role_id)
);

create table IF NOT EXISTS connection_contractor_providers
(
    contractor_id          UUID,
    provider_id            UUID,
    status_connection      int, --//статус: подрядчик, поставщик
    primary key (contractor_id, provider_id),
    foreign key (contractor_id) references companies (company_id),
    foreign key (provider_id) references companies (company_id),
    foreign key (status_connection) references roles (role_id)
);

--//таблица связей компании и project_manager
create table IF NOT EXISTS companies_pm
(
    company_id             UUID not null,
    user_id                UUID not null,
    primary key (company_id, user_id),
    foreign key (user_id) references users (user_id),
    foreign key (company_id) references companies (company_id)
);



insert into connection_contractor_customers (contractor_id, customer_id, status_connection)
values ('68b6c777-f9ab-4d66-add4-d00c3b1a69c3', '68b6c777-f9ab-4d66-add4-d00c3b1a69c3', 1),
       ('68b6c777-f9ab-4d66-add4-d00c3b1a69c3', '910bd89c-7c52-41e3-916b-6e7c03e8d11b', 2);

insert into connection_contractor_providers (contractor_id, provider_id, status_connection)
values ('8b82150a-4f0a-4ebf-9ee0-0a2b5fd9e281', '8b82150a-4f0a-4ebf-9ee0-0a2b5fd9e281', 1),
       ('8b82150a-4f0a-4ebf-9ee0-0a2b5fd9e281', '910bd89c-7c52-41e3-916b-6e7c03e8d11b', 2);

insert into companies_pm (company_id, user_id) values
        ('68b6c777-f9ab-4d66-add4-d00c3b1a69c3', '797e5399-e280-44bc-b9ae-d422ee45afde'),
        ('68b6c777-f9ab-4d66-add4-d00c3b1a69c3', '22dc87ab-2cea-43ed-ab20-12def235bccc'),
        ('910bd89c-7c52-41e3-916b-6e7c03e8d11b', 'dc4d798b-44e1-4edf-9e8e-8dc8043ab472'),
        ('910bd89c-7c52-41e3-916b-6e7c03e8d11b', '94634392-9bf3-4329-be9e-c487c1da4d2d');



insert into company_specializations( company_id, specialization_id) values
        ('68b6c777-f9ab-4d66-add4-d00c3b1a69c3', 1),
        ('68b6c777-f9ab-4d66-add4-d00c3b1a69c3', 4),
        ('910bd89c-7c52-41e3-916b-6e7c03e8d11b', 2),
        ('910bd89c-7c52-41e3-916b-6e7c03e8d11b', 3),
        ('8b82150a-4f0a-4ebf-9ee0-0a2b5fd9e281', 4);



insert into profile_companies (company_id) values
        ('68b6c777-f9ab-4d66-add4-d00c3b1a69c3'),
        ('910bd89c-7c52-41e3-916b-6e7c03e8d11b'),
        ('8b82150a-4f0a-4ebf-9ee0-0a2b5fd9e281'),
        ('8b82150a-4f0a-4ebf-9ee0-0a2b5fd9e281');