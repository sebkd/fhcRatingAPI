CREATE TABLE IF NOT EXISTS files_info (
    id              bigserial PRIMARY KEY,
    owner_id        UUID references users (user_id),
    name            varchar(255),
    description     varchar(255),
    size            bigint,
    external_id     varchar(255),
    external_path   varchar(255),
    created_at      timestamp DEFAULT current_timestamp,
    updated_at      timestamp DEFAULT current_timestamp
);