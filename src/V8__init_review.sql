create table IF NOT EXISTS review (
        id              bigserial primary key,
        author          UUID references users (user_id),
        title           varchar(255) not null,
        dates           timestamp,
        description     varchar(2000)
);

insert into review (author, title, description) values
('797e5399-e280-44bc-b9ae-d422ee45afde', 'работа на объекте', 'Работа выполнена отвратительно, все двери стоят по уровню а окна на месте' ),
('22dc87ab-2cea-43ed-ab20-12def235bccc', 'работа в поле', 'Да ниче так трудются' );