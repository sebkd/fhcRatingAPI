--//таблица компаний
create table IF NOT EXISTS companies
(
    company_id             UUID primary key,
    company_name           varchar(255) not null,
    inn                    int,
    kpp                    int,
    general_manager        UUID       not null,
    legal_address          varchar(255),
    actual_address         varchar(255),
    company_phone          varchar(255),
    company_email          varchar(255),
    number_Employees       int,
    description            varchar,
    foreign key (general_manager) references users (user_id)
);

insert into companies (company_id, company_name,inn, kpp, general_manager, legal_address, number_Employees)
values ('68b6c777-f9ab-4d66-add4-d00c3b1a69c3','Capital Group', 123445, 2312,  '797e5399-e280-44bc-b9ae-d422ee45afde', 'Россия, Москва', 250),
       ('910bd89c-7c52-41e3-916b-6e7c03e8d11b','Design Company',123445, 2312,  'dc4d798b-44e1-4edf-9e8e-8dc8043ab472', 'Италия, Рим', 4),
       ('8b82150a-4f0a-4ebf-9ee0-0a2b5fd9e281','Engineering', 123445, 2312,  '7895f5bd-a201-4709-8886-5acdfe824163', 'Россия, Тула', 26),
       ('3c656aae-be15-45c5-8188-e12e761b32f5','Компания Ника: мебель, текстиль', 123445, 2312,  '5a4e237b-6413-4a1d-8eee-9cc5754661e8', 'Россия, Москва', 12);


--//таблица профилей компаний
create table IF NOT EXISTS profile_companies
(
    profile_id             bigserial primary key,
    company_id             UUID,
--    specialization_id      bigint,
    foreign key (company_id) references companies (company_id)
--    foreign key (specialization_id) references specializations (specialization_id)
);