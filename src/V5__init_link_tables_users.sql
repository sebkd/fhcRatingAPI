--//кросс таблица пользователей, ролей
CREATE TABLE IF NOT EXISTS users_roles (
    user_id                UUID NOT NULL,
    role_id                int NOT NULL,
    PRIMARY KEY (user_id, role_id),
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    FOREIGN KEY (role_id) REFERENCES roles (role_id)
);


INSERT INTO users_roles (user_id, role_id) VALUES
('797e5399-e280-44bc-b9ae-d422ee45afde', 1),
('797e5399-e280-44bc-b9ae-d422ee45afde', 2),
('22dc87ab-2cea-43ed-ab20-12def235bccc', 1),
('22dc87ab-2cea-43ed-ab20-12def235bccc', 2),
('dc4d798b-44e1-4edf-9e8e-8dc8043ab472', 1),
('dc4d798b-44e1-4edf-9e8e-8dc8043ab472', 2);

CREATE TABLE IF NOT EXISTS lk_user_specializations (
    profile_id              bigint NOT NULL,
    specialization_id       bigint NOT NULL,
    PRIMARY KEY (profile_id, specialization_id),
    FOREIGN KEY (profile_id) REFERENCES profile_users(profile_id),
    FOREIGN KEY (specialization_id) REFERENCES specializations (id)
);


INSERT INTO lk_user_specializations ( profile_id, specialization_id) VALUES
(1, 1), (1, 2), (1, 3), (1, 4), (1, 5),
(2, 8), (2, 9), (2, 10), (2, 11),
(3, 2), (3, 3),
(4, 6);

CREATE TABLE IF NOT EXISTS city_profiles (
    profile_id              bigint NOT NULL,
    city_id                 bigint NOT NULL,
    PRIMARY KEY (profile_id, city_id),
    FOREIGN KEY (profile_id) REFERENCES profile_users(profile_id),
    FOREIGN KEY (city_id) REFERENCES geo_regions (id)
);


INSERT INTO city_profiles(profile_id, city_id) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 3);


