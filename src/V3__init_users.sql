--//таблица пользователей сервиса
CREATE TABLE IF NOT EXISTS users (
    user_id                 UUID PRIMARY KEY,
    user_name               varchar(30),
    user_surname            varchar(30),
    user_patronymic         varchar(30),
    user_password           varchar(80) NOT NULL,
    user_email              varchar(50) NOT NULL,
    user_telegram           varchar(50),
    user_phone              varchar(30),
    user_whatsapp           varchar(30),
    user_photo              varchar(255)
);


INSERT INTO users (user_id, user_surname, user_name, user_patronymic, user_password, user_email, user_telegram,  user_phone, user_whatsapp) VALUES
('797e5399-e280-44bc-b9ae-d422ee45afde', 'Иванов', 'Петр', 'Иванович','$2a$12$zCa/vu1142NwfLnRW..e0.wdnmxgnh7if6ROeVuCi62XR6N9db9eO', 'petya@google.com', '@petrIvan', 89851002121, 89851002121);

INSERT INTO users (user_id, user_surname, user_name, user_patronymic, user_password, user_email, user_telegram,  user_phone, user_whatsapp) VALUES
('22dc87ab-2cea-43ed-ab20-12def235bccc', 'Васев', 'Василий', 'Петрович', '$2a$12$zCa/vu1142NwfLnRW..e0.wdnmxgnh7if6ROeVuCi62XR6N9db9eO', 'vasya@google.com', '@VVP', 89851002122, 89851002122);

INSERT INTO users (user_id, user_name, user_password, user_email, user_phone) VALUES
('dc4d798b-44e1-4edf-9e8e-8dc8043ab472', 'Misha', '$2a$12$zCa/vu1142NwfLnRW..e0.wdnmxgnh7if6ROeVuCi62XR6N9db9eO', 'misha@google.com', 89851002123),
('94634392-9bf3-4329-be9e-c487c1da4d2d', 'Lesha', '$2a$12$zCa/vu1142NwfLnRW..e0.wdnmxgnh7if6ROeVuCi62XR6N9db9eO', 'lesha@google.com', 89851002124),
('7895f5bd-a201-4709-8886-5acdfe824163', 'Ksenya', '$2a$12$zCa/vu1142NwfLnRW..e0.wdnmxgnh7if6ROeVuCi62XR6N9db9eO', 'ksenya@google.com', 89851002125),
('5a4e237b-6413-4a1d-8eee-9cc5754661e8', 'Olya', '$2a$12$zCa/vu1142NwfLnRW..e0.wdnmxgnh7if6ROeVuCi62XR6N9db9eO', 'olya@google.com', 89851002126);

--//таблица личных кабинетов пользователей
CREATE TABLE IF NOT EXISTS profile_users (
    profile_id             bigserial PRIMARY KEY,
    user_id                UUID NOT NULL,
    user_position          varchar(255)
);

INSERT INTO profile_users (user_id) VALUES
('dc4d798b-44e1-4edf-9e8e-8dc8043ab472'),
('94634392-9bf3-4329-be9e-c487c1da4d2d'),
('7895f5bd-a201-4709-8886-5acdfe824163'),
('5a4e237b-6413-4a1d-8eee-9cc5754661e8');
