create table IF NOT EXISTS status (
        status_id           bigserial primary key,
        title               varchar(20)
);

create table IF NOT EXISTS type_client(
        id                  serial primary key,
        type                varchar(50)
);

insert into type_client(type) values ('OOO'),('IP'),('ZAO'),('AO'),('CHL');

create table IF NOT EXISTS clients(
        id                  bigserial primary key,
        type                int not null references type_client(id),
        client_id           UUID
);

insert into clients(type, client_id) values
(1, '68b6c777-f9ab-4d66-add4-d00c3b1a69c3'),
(2, '68b6c777-f9ab-4d66-add4-d00c3b1a69c3'),
(3, 'dc4d798b-44e1-4edf-9e8e-8dc8043ab472'),
(5,'94634392-9bf3-4329-be9e-c487c1da4d2d');

create table IF NOT EXISTS tenders(
        tender_id           bigserial primary key,
        title               varchar(100) not null,
        data_start          varchar(20),
        address             varchar(100),
        description         varchar(1000),
        contractor_id       bigint,
        customer_id         bigint not null,
        price               bigint,
        status_id           bigint,
        estimate_id         bigint,
--        platform_id         bigint,
        foreign key (contractor_id) references clients(id),
        foreign key (customer_id) references clients(id),
        foreign key (status_id) references status(status_id),
        foreign key (estimate_id) references estimates(estimate_id)
--        foreign key (platform_id) references platforms (platform_id)
);

insert into status (title) values
('черновик'),
('объявлен тендер'),
('тендер завершен'),
('в работе'),
('ждет подтверждения');

insert into estimates (company_id, estimate_number, estimate_sum) values
('68b6c777-f9ab-4d66-add4-d00c3b1a69c3', 1, 1000),
('68b6c777-f9ab-4d66-add4-d00c3b1a69c3',2, 2000),
('910bd89c-7c52-41e3-916b-6e7c03e8d11b',3, 3000),
('910bd89c-7c52-41e3-916b-6e7c03e8d11b',4, 4000);

insert into tenders (title, customer_id, contractor_id, price, status_id, address) values
('Строительство коттеджа 600м2', 1, 4, '20000000', 2, 'Московская область, Новорижское шоссе, снт Итренок'),
('Строительство коттеджа 400м2', 1,2, '500000', 4, 'Московская область, Калужское шоссе'),
('Строительство коттеджа 200м2', 2,4, '1000000', 2, 'Московская область, Киевское шоссе'),
('Строительство коттеджа 120м2', 2,1, '100000', 4, 'Московская область, Киевское шоссе'),
('Ремонт квартиры 250м2', 3,null, '1200000', 2, 'Москва, Малая ордынка, 19'),
('Ремонт квартиры 140м2', 2,1, '1500000', 3, 'Москва, Старый арбат, 56'),
('Ремонт квартиры 290м2', 1,2, '1700000', 4, 'Москва, Мосфильмовская, 35'),
('Ремонт квартиры 340м2', 2,null, '1900000', 2, 'Москва, Краснопролетарская, 7'),
('Ремонт квартиры 450м2', 3,1, '22100000', 3, 'Москва, Месницкая, 11'),
('Дизайнерский ремонт квартиры 350м2', 1,null, '2500000', 3, 'Москва, ул Минская 13');





