CREATE TABLE IF NOT EXISTS price_items(
        item_id             bigserial PRIMARY KEY,
        operation_id        bigint,
        price               bigint
--        foreign key (operation_id) references operations (operation_id)
);

CREATE TABLE IF NOT EXISTS prices (
        price_id            bigserial PRIMARY KEY,
        client_id           UUID NOT NULL
);

CREATE TABLE IF NOT EXISTS pricelist (
        price_id        bigint NOT NULL,
        item_id         bigint NOT NULL,
        PRIMARY KEY (price_id, item_id),
        FOREIGN KEY (price_id) REFERENCES prices(price_id),
        FOREIGN KEY (item_id) REFERENCES price_items (item_id)
);

INSERT INTO price_items (operation_id, price) VALUES
(1, 1000), (1, 1200), (1, 1300),
(2, 2000), (2, 2200), (2, 2300),
(3, 3000), (3, 3200), (3, 3300);

INSERT INTO prices (client_id) VALUES
('dc4d798b-44e1-4edf-9e8e-8dc8043ab472'),
('94634392-9bf3-4329-be9e-c487c1da4d2d'),
('7895f5bd-a201-4709-8886-5acdfe824163'),
('5a4e237b-6413-4a1d-8eee-9cc5754661e8');

INSERT INTO pricelist (price_id, item_id) VALUES
(1, 1), (1, 4), (1, 7),
(2, 2), (2, 5), (2, 8),
(3, 3), (3, 6), (3, 9),
(4, 1), (4, 2), (4, 6);
