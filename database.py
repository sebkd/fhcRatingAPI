"""
модуль подключения к базе
"""
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# указываем путь к базе к контейнеру
# здесь нужно изменить admin на postgres, postgres - на название образа
# SQLALCHEMY_DATABASE_URL = "postgresql://admin:123456789@0.0.0.0:5432/fhc"
# SQLALCHEMY_DATABASE_URL = "postgresql://admin:123456789@postgres/fhc"
SQLALCHEMY_DATABASE_URL = "postgresql://postgres:1234567890@postgres-fhc/fhc"

# запускаем движок
engine = create_engine(
    SQLALCHEMY_DATABASE_URL
)
# запускаем сессию
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# возвращаем класс который будем использовать при создании моделей
Base = declarative_base()
