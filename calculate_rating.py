"""
у кандидата должно быть
общее значение (главная звездочка) old_general_value
общее количество лайков old_number_likes
old_general_value = (3.8, 15)
old_intermed_values = ((5, 24), (2, 34), (1, 4), (5, 67), (2, 13))
new_general_star = (3)
в этом случае выход
new_general_value = (3.75, 16)
new_intermed_values = ((5, 24), (2, 34), (1, 4), (5, 67), (2, 13)) - не меняются

или
значения по каждому показателю и лайки по ним
old_general_value = (3.8, 15)
new_general_star = (0) - равна нулю
old_intermed_values = ((5, 24), (2, 34), (1, 4), (5, 67), (2, 13))
new_intermed_star = (5, 3, 2, 1, 3)
по каждому показателю ведется статистика
в этом случае выход
new_general_value = (3.74, 16)
new_intermed_values = ((5, 25), (2.03, 35), (1.2, 5), (4.94, 68), (2.07, 14))

Подсчет рейтинга
Рейтинг считается по следующему алгоритму
- если нет общего рейтинга или есть рейтинги по остальным параметрам, то
расчет ведется по остальным параметрам иначе ведется расчет по общему рейтингу
1) каждый параметр имеет показатель предыдущих расчетов и учитывается в статистике
существующее значение по формуле
Rgen = ( Rgen * n + Rnew ) / (n + 1)
если ранее по данному показателю было 0,6 балла и до этого лайков было 11,
а новый пользователь дал 4 звезды,то
новый рейтинг по показателю
Rgen = ( 0,6 * 11 + 0,8 ) / (11 + 1) = 0,6166
2) для расчета генерального показателя (общего по всем), каждый параметр считается как
5 звезд = 1 балл
4 звезды = 0,8 баллов
3 звезды = 0,6 баллов
2 звезды = 0,4 балла
1 звезда = 0,2 балла
например
если из 5 параметров поставили лайк только по 2 параметрам, то расчет необходимо производить
только по этим двум параметрам, как среднее арифметическое
"""


class Rank:
    def __init__(self, old_general_value, old_intermed_values, new_general_star, new_intermed_star):
        """
        Подсчет рейтинга
        :type new_intermed_star: object
        :type new_general_star: object
        :type old_intermed_values: object
        :type old_general_value: object
        """
        self.old_general_value = old_general_value
        self.old_intermed_values = old_intermed_values
        self.new_general_star = new_general_star
        self.new_intermed_star = new_intermed_star

        # новые показатели которые необходимо забирать
        self.new_general_value = []
        self.new_intermed_values = []

    @staticmethod
    def return_local_value(number_stars):
        """
        Функция пересчет звезд в балльное значение
        :param number_stars: кол-во звезд
        :return: балльное значение * 5
        """
        if 0 < number_stars < 6:
            local_value = 0
            if number_stars == 5:
                local_value = 1
            elif number_stars == 4:
                local_value = 0.8
            elif number_stars == 3:
                local_value = 0.6
            elif number_stars == 2:
                local_value = 0.4
            elif number_stars == 1:
                local_value = 0.2
            return local_value * 5
        else:
            return 0

    @staticmethod
    def calculate(general_value, number_likes, local_value):
        """

        Функция подсчета общего рейтинга по формуле
        Rgen = ( Rgen * n + Rnew ) / (n + 1)
        :param local_value:
        :param general_value: предыдущее общее значение рейтинга из БД
        :param number_likes: количество лайков из БД
        :return: новый общий рейтинг, новое количество лайков
        """

        return round((general_value * number_likes + local_value) / (number_likes + 1), 2), number_likes + 1

    @staticmethod
    def calculate_intermed_to_gen_value(intermed_values):
        """
        Расчет среднего по промежуточным значениям
        :param intermed_values:
        :return:
        """
        return round(sum(intermed_values) / len(intermed_values), 1)

    def calc_gen_value_by_general_value(self, old_value, old_number_likes, new_general_star):
        """
        Расчет среднего общего значения (главная звездочка), расчет если поставлен лайк на главной звезде
        :type new_general_star: object
        :param old_value: старое значение звездочки float
        :param old_number_likes: число лайков до int
        :param new_general_star: новая звездочка поставленная int
        :return: новое значение звездочки, новое число лайков float, int
        """
        local_value = self.return_local_value(new_general_star)
        return self.calculate(old_value, old_number_likes, local_value)

    def calc_gen_value_by_intermed_values(self, old_value, old_number_likes, new_intermed_star):
        """
        Расчет среднего общего значения (главная звездочка),
        расчет если поставлены лайки на промежуточных звездах
        :param old_value: старое значение звездочки float
        :param old_number_likes: число лайков до int
        :param new_intermed_star: список оценок по параметрам list[int, int,...]
        :return: новое значение звездочки, новое число лайков float, int
        """
        local_value = self.calculate_intermed_to_gen_value(new_intermed_star)
        return self.calculate(old_value, old_number_likes, local_value)

    def calc_intermed_values(self, old_intermed_values, new_intermed_star):
        """
        Расчет новых показателей значение/кол-во лайков для промежуточных показателей
        :type new_intermed_star: object
        :type old_intermed_values: object
        :param old_intermed_values: старые значения list[[float, int],[float, int],...]
        :param new_intermed_star: новые значения звезд [int, int, ...]
        :return: новые значения list[[float, int],[float, int],...]
        """
        new_intermed_values = []
        for index, inter_value in enumerate(old_intermed_values):
            new_intermed_values.append(list(self.calc_gen_value_by_general_value(old_value=inter_value[0],
                                                                                 old_number_likes=inter_value[1],
                                                                                 new_general_star=new_intermed_star[
                                                                                     index])))

        return new_intermed_values

    def run(self):
        """
        Главная функция
        :return:
        """
        if self.new_general_star:
            self.new_general_value = list(self.calc_gen_value_by_general_value(old_value=self.old_general_value[0],
                                                                               old_number_likes=self.old_general_value[
                                                                                   1],
                                                                               new_general_star=self.new_general_star))
            self.new_intermed_values = self.old_intermed_values
        else:
            self.new_general_value = list(self.calc_gen_value_by_intermed_values(old_value=self.old_general_value[0],
                                                                                 old_number_likes=
                                                                                 self.old_general_value[1],
                                                                                 new_intermed_star=
                                                                                 self.new_intermed_star))
            self.new_intermed_values = self.calc_intermed_values(old_intermed_values=self.old_intermed_values,
                                                                 new_intermed_star=self.new_intermed_star)

    def get_new_params(self):
        """
        Выходные данные
        :return: [3.74, 16], [[5.0, 25], [2.03, 35], [1.2, 5], [4.94, 68], [2.07, 14]]
        """
        return self.new_general_value, self.new_intermed_values


def main():
    old_general_value = [3.8, 15]
    # old_general_value = [3.8, 10]
    old_intermed_values = [[5, 24], [2, 34], [1, 4], [5, 67], [2, 13]]
    # old_intermed_values = [[4.1, 14], [3.0, 13], [5.0, 10], [4.2, 6], [2.0, 9]]
    new_general_star = 4
    # new_general_star = 0
    new_intermed_star = [0, 0, 0, 0, 0]
    # new_intermed_star = [5, 3, 2, 1, 3]

    rank_user = Rank(old_general_value=old_general_value,
                     old_intermed_values=old_intermed_values,
                     new_general_star=new_general_star,
                     new_intermed_star=new_intermed_star)
    rank_user.run()
    print(old_general_value, old_intermed_values)
    print(new_general_star, new_intermed_star)
    print(rank_user.get_new_params())
    print()


if __name__ == '__main__':
    main()
