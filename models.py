"""
модуль создания моделей
"""
from sqlalchemy.schema import Table, MetaData

# пользовательские импорты
from database import Base, engine

# опредеяем объект метаданных
metadata = MetaData(engine)


class Rating(Base):
    """
    класс рейтингов, просто забираем уже с готовой БД
    """
    __table__ = Table('rating', metadata, autoload_with=engine)


class SetEval(Base):
    """
    класс оценок, просто забираем уже с готовой БД
    """
    __table__ = Table('set_eval', metadata, autoload_with=engine)
