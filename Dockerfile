from python:latest

RUN apt-get update && apt-get install -y cron

RUN pip3 install --upgrade pip

WORKDIR /code

COPY ./ /code
COPY ./requirements.txt /code/requirements.txt
RUN pip3 install --no-cache-dir --upgrade -r /code/requirements.txt

COPY cron_fhc_rating /etc/cron.d/cron_fhc_rating
RUN chmod 0644 /etc/cron.d/cron_fhc_rating && crontab /etc/cron.d/cron_fhc_rating
COPY ./app /code/app

CMD service cron start

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
